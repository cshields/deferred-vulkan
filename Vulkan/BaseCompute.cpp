#include "BaseCompute.h"
#include "VulkanCore.h"

BaseCompute::~BaseCompute()
{
}

void BaseCompute::prepareComputePipelines()
{
  // Create compute shader pipelines
  VkComputePipelineCreateInfo computePipelineCreateInfo =
    vkTools::initializers::computePipelineCreateInfo(
      m_computePipelineLayout,
      0);

  computePipelineCreateInfo.stage = shaderStages[0];

  VkResult err = vkCreateComputePipelines(*m_core->getDevice(), pipelineCache, 1, &computePipelineCreateInfo, nullptr, &m_pipeline);
  assert(!err);
}