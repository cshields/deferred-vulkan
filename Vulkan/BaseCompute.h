#pragma once
#include "BaseShader.h"

class VulkanCore;

class BaseCompute : public Shader
{
public:
  BaseCompute(VulkanCore* core) : Shader(core) {}
  ~BaseCompute();

  void prepareComputePipelines();

private:
  VkPipelineLayout m_computePipelineLayout;
  VkDescriptorSet m_computeDescriptorSet;
  VkDescriptorSetLayout m_computeDescriptorSetLayout;
  VkDescriptorPool m_computeDescriptorPool;
};

