#include "BaseShader.h"
#include "VulkanCore.h"

Shader::Shader(VulkanCore* core)
{
  m_core = core;

  VkPipelineCacheCreateInfo pipelineCacheCreateInfo = {};
  pipelineCacheCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
  VkResult err = vkCreatePipelineCache(*m_core->getDevice(), &pipelineCacheCreateInfo, nullptr, &pipelineCache);
  assert(!err);
}


Shader::~Shader()
{
  vkDestroyDescriptorPool(*m_core->getDevice(), descriptorPool, nullptr);

  vkDestroyDescriptorSetLayout(*m_core->getDevice(), descriptorSetLayout, nullptr);

  vkDestroyPipelineLayout(*m_core->getDevice(), pipelineLayout, nullptr);

  vkDestroyPipeline(*m_core->getDevice(), m_pipeline, nullptr);

  vkDestroyPipelineCache(*m_core->getDevice(), pipelineCache, nullptr);
}

void Shader::setupDescriptorSetLayout()
{
  setupDescriptorSetLayout(0, nullptr);
}

void Shader::setupDescriptorSetLayout(int pushCount, VkPushConstantRange *ptr)
{
  std::vector<VkDescriptorSetLayoutBinding> setLayoutBindings;

  for (int i = 0; i < bindingDescriptors.size(); i++) {
    setLayoutBindings.push_back(vkTools::initializers::descriptorSetLayoutBinding(
      bindingDescriptors[i].type,
      bindingDescriptors[i].shaderStage,
      bindingDescriptors[i].binding));
  }

  VkDescriptorSetLayoutCreateInfo descriptorLayout =
    vkTools::initializers::descriptorSetLayoutCreateInfo(
      setLayoutBindings.data(),
      setLayoutBindings.size());

  VkResult err = vkCreateDescriptorSetLayout(*m_core->getDevice(), &descriptorLayout, NULL, &descriptorSetLayout);
  assert(!err);

  VkPipelineLayoutCreateInfo pPipelineLayoutCreateInfo =
    vkTools::initializers::pipelineLayoutCreateInfo(
      &descriptorSetLayout,
      1);

  pPipelineLayoutCreateInfo.pushConstantRangeCount = pushCount;
  pPipelineLayoutCreateInfo.pPushConstantRanges = ptr;

  err = vkCreatePipelineLayout(*m_core->getDevice(), &pPipelineLayoutCreateInfo, nullptr, &pipelineLayout);
  assert(!err);
}

void Shader::preparePipelines(const VkRenderPass &renderPass)
{
  // Vertex input state
  // Describes the topology used with this pipeline
  VkPipelineInputAssemblyStateCreateInfo inputAssemblyState =
    vkTools::initializers::pipelineInputAssemblyStateCreateInfo(
      VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
      0,
      VK_FALSE);

  // Rasterization state
  VkPipelineRasterizationStateCreateInfo rasterizationState =
    vkTools::initializers::pipelineRasterizationStateCreateInfo(
      VK_POLYGON_MODE_FILL,
      m_faceCulling,
      VK_FRONT_FACE_CLOCKWISE,
      0);

  VkPipelineColorBlendStateCreateInfo colorBlendState =
    vkTools::initializers::pipelineColorBlendStateCreateInfo(
      blendAttachmentStates.size(),
      blendAttachmentStates.data());

  // Viewport state
  VkPipelineViewportStateCreateInfo viewportState =
    vkTools::initializers::pipelineViewportStateCreateInfo(1, 1, 0);


  // Enable dynamic states
  // Describes the dynamic states to be used with this pipeline
  // Dynamic states can be set even after the pipeline has been created
  // So there is no need to create new pipelines just for changing
  // a viewport's dimensions or a scissor box
  std::vector<VkDynamicState> dynamicStateEnables = {
    VK_DYNAMIC_STATE_VIEWPORT,
    VK_DYNAMIC_STATE_SCISSOR
  };
  VkPipelineDynamicStateCreateInfo dynamicState =
    vkTools::initializers::pipelineDynamicStateCreateInfo(
      dynamicStateEnables.data(),
      dynamicStateEnables.size(),
      0);

  // Depth and stencil state
  // Describes depth and stenctil test and compare ops
  VkPipelineDepthStencilStateCreateInfo depthStencilState =
    vkTools::initializers::pipelineDepthStencilStateCreateInfo(
      VK_TRUE,
      m_depthWrite,
      VK_COMPARE_OP_LESS_OR_EQUAL);

  // Multi sampling state
  VkPipelineMultisampleStateCreateInfo multisampleState =
    vkTools::initializers::pipelineMultisampleStateCreateInfo(
      VK_SAMPLE_COUNT_1_BIT,
      0);

  VkGraphicsPipelineCreateInfo pipelineCreateInfo =
    vkTools::initializers::pipelineCreateInfo(
      pipelineLayout,
      renderPass,
      0);


  // Assign states
  // Two shader stages
  pipelineCreateInfo.stageCount = shaderStages.size();
  // Assign pipeline state create information
  pipelineCreateInfo.pVertexInputState = &inputState;
  pipelineCreateInfo.pInputAssemblyState = &inputAssemblyState;
  pipelineCreateInfo.pRasterizationState = &rasterizationState;
  pipelineCreateInfo.pColorBlendState = &colorBlendState;
  pipelineCreateInfo.pMultisampleState = &multisampleState;
  pipelineCreateInfo.pViewportState = &viewportState;
  pipelineCreateInfo.pDepthStencilState = &depthStencilState;
  pipelineCreateInfo.pStages = shaderStages.data();
  pipelineCreateInfo.pDynamicState = &dynamicState;

  // Create rendering pipeline
  VkResult err = vkCreateGraphicsPipelines(*m_core->getDevice(), pipelineCache, 1, &pipelineCreateInfo, nullptr, &m_pipeline);
  assert(!err);
}

void Shader::setupDescriptorPool()
{
  setupDescriptorPool(1);
}
void Shader::setupDescriptorPool(int maxSets)
{
  VkDescriptorPoolCreateInfo descriptorPoolInfo =
    vkTools::initializers::descriptorPoolCreateInfo(
      poolSizes.size(),
      poolSizes.data(),
      maxSets);

  VkResult vkRes = vkCreateDescriptorPool(*m_core->getDevice(), &descriptorPoolInfo, nullptr, &descriptorPool);
  assert(!vkRes);
}

void Shader::setupDescriptorSet()
{
  VkDescriptorSetAllocateInfo allocInfo =
    vkTools::initializers::descriptorSetAllocateInfo(
      descriptorPool,
      &descriptorSetLayout,
      1);

  descriptorSets.resize(1);
  VkResult vkRes = vkAllocateDescriptorSets(*m_core->getDevice(), &allocInfo, &descriptorSets[0]);
  assert(!vkRes);

  std::vector<VkWriteDescriptorSet> writeDescriptorSets;
  for (int i = 0; i < bindingDescriptors.size(); i++)
    writeDescriptorSets.push_back(updateDescriptorSet(bindingDescriptors[i], descriptorSets[0]));

  vkUpdateDescriptorSets(*m_core->getDevice(), writeDescriptorSets.size(), writeDescriptorSets.data(), 0, NULL);
}

void Shader::setupDescriptorSet(const std::vector<vkMeshLoader::MeshBuffer> &vertexBuffs)
{
  VkDescriptorSetAllocateInfo allocInfo =
    vkTools::initializers::descriptorSetAllocateInfo(
      descriptorPool,
      &descriptorSetLayout,
      1);

  descriptorSets.resize(vertexBuffs.size());
  std::vector<VkWriteDescriptorSet> writeDescriptorSets;
  for (int j = 0; j < vertexBuffs.size(); j++) {
    VkResult vkRes = vkAllocateDescriptorSets(*m_core->getDevice(), &allocInfo, &descriptorSets[j]);
    assert(!vkRes);

    bindingDescriptors[1].imgDescriptor = const_cast<VkDescriptorImageInfo*>(&vertexBuffs[j].m_diffTex.texDescriptor);

    for (int i = 0; i < bindingDescriptors.size(); i++) {
      switch (i) {
      case 2:
        if (vertexBuffs[j].m_specTex.image == VK_NULL_HANDLE)
          continue;
        bindingDescriptors[2].imgDescriptor = const_cast<VkDescriptorImageInfo*>(&vertexBuffs[j].m_specTex.texDescriptor);
        break;
      case 3:
        if (vertexBuffs[j].m_normTex.image == VK_NULL_HANDLE)
          continue;
        bindingDescriptors[3].imgDescriptor = const_cast<VkDescriptorImageInfo*>(&vertexBuffs[j].m_normTex.texDescriptor);
        break;
      }
      writeDescriptorSets.push_back(updateDescriptorSet(bindingDescriptors[i], descriptorSets[j]));
    }
  }

  vkUpdateDescriptorSets(*m_core->getDevice(), writeDescriptorSets.size(), writeDescriptorSets.data(), 0, NULL);
}

VkWriteDescriptorSet Shader::updateDescriptorSet(bindingDescriptor &descriptor, VkDescriptorSet &set)
{
  if (descriptor.type == VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER)
    return vkTools::initializers::writeDescriptorSet(
      set,
      descriptor.type,
      descriptor.binding,
      descriptor.uniDescriptor);
  else
    return vkTools::initializers::writeDescriptorSet(
      set,
      descriptor.type,
      descriptor.binding,
      descriptor.imgDescriptor);
}

void Shader::mapUniform(VkDeviceMemory mem, uint32_t size, void* data)
{
  uint8_t *pData;
  // Map uniform buffer and update it
  VkResult err = vkMapMemory(*m_core->getDevice(), mem, 0, size, 0, (void **)&pData);
  assert(!err);
  memcpy(pData, data, size);
  vkUnmapMemory(*m_core->getDevice(), mem);
  assert(!err);
}

void Shader::setInputState()
{
  // Assign to vertex buffer
  inputState = vkTools::initializers::pipelineVertexInputStateCreateInfo();
  inputState.vertexBindingDescriptionCount = bindingDescriptions.size();
  inputState.pVertexBindingDescriptions = bindingDescriptions.data();
  inputState.vertexAttributeDescriptionCount = attributeDescriptions.size();
  inputState.pVertexAttributeDescriptions = attributeDescriptions.data();
}