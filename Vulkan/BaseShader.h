#pragma once
#include <glm\glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "vulkanTextureLoader.hpp"
#include "vulkanMeshLoader.hpp"
#include <unordered_map>

class VulkanCore;

class Shader
{
public:
  struct bindingDescriptor {
    VkDescriptorType type;
    VkShaderStageFlagBits shaderStage;
    uint32_t binding;
    VkDescriptorBufferInfo *uniDescriptor;
    VkDescriptorImageInfo *imgDescriptor;

    bindingDescriptor(VkDescriptorType a, VkShaderStageFlagBits b, uint32_t c, VkDescriptorBufferInfo *d)
    {
      type = a;
      shaderStage = b;
      binding = c;
      uniDescriptor = d;
    }

    bindingDescriptor(VkDescriptorType a, VkShaderStageFlagBits b, uint32_t c, VkDescriptorImageInfo *d)
    {
      type = a;
      shaderStage = b;
      binding = c;
      imgDescriptor = d;
    }
  };

  Shader() {};
  Shader(VulkanCore *core);
  ~Shader();

  VkDescriptorSet* getSet(int setID = 0) { return &descriptorSets[setID]; }
  VkDescriptorSetLayout* getLayout() { return &descriptorSetLayout; }
  VkPipelineLayout* getPipelineLayout() { return &pipelineLayout; }
  VkPipeline* getPipeline() { return &m_pipeline; }
  void addTexInfo(uint32_t binding, VkDescriptorImageInfo texInfo) { boundTextures.emplace(binding, texInfo); }

  virtual void setupDescriptorSetLayout();
  void setupDescriptorSetLayout(int pushCount, VkPushConstantRange *ptr);
  virtual void setupDescriptorPool();
  void setupDescriptorPool(int maxSets);
  virtual void setupDescriptorSet();
  void setupDescriptorSet(const std::vector<vkMeshLoader::MeshBuffer> &vertexBuffs);
  VkWriteDescriptorSet updateDescriptorSet(bindingDescriptor &descriptor, VkDescriptorSet &set);
  virtual void prepareUniformBuffers() = 0;
  virtual void updateUniformBuffers() = 0;
  virtual void preparePipelines(const VkRenderPass &renderPass);

  void mapUniform(VkDeviceMemory mem, uint32_t size, void* data);
  void setInputState();

protected:
  std::vector<bindingDescriptor> bindingDescriptors;
  std::vector<VkDescriptorPoolSize> poolSizes;
  std::vector<VkPipelineShaderStageCreateInfo> shaderStages;
  std::vector<VkPipelineColorBlendAttachmentState> blendAttachmentStates;

  std::unordered_map<uint32_t, VkDescriptorImageInfo> boundTextures;

  std::vector<VkDescriptorSet> descriptorSets;
  VkDescriptorSetLayout descriptorSetLayout;

  VkDescriptorPool descriptorPool;
  VkPipelineVertexInputStateCreateInfo inputState;
  std::vector<VkVertexInputBindingDescription> bindingDescriptions;
  std::vector<VkVertexInputAttributeDescription> attributeDescriptions;

  VkPipelineLayout pipelineLayout;
  VkPipelineCache pipelineCache;
  VkPipeline m_pipeline;
  uint32_t m_faceCulling = VK_CULL_MODE_BACK_BIT;
  VkBool32 m_depthWrite = VK_TRUE;

  int m_numDescriptorSets = 1;
  void setNumSets(int num) { m_numDescriptorSets = num; }

  VulkanCore* m_core;
};

