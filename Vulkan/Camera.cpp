#include "Camera.h"


Camera::Camera(VulkanCore* core, float angle, float aspect, float nearClip, float farClip)
{
  m_core = core;
  m_projection = glm::perspective(angle, aspect, nearClip, farClip);
  eye = glm::vec3(0.f);
  up = facing = glm::vec3(0.f, 1.f, 0.f);

}

Camera::~Camera()
{
}

glm::mat4 Camera::changeProjection(float angle, float aspect, float nearClip, float farClip)
{
 return m_projection = glm::perspective(angle, aspect, nearClip, farClip);
}

void Camera::Orientate()
{
  at = facing = glm::vec3(0.f, 0.f, -1.f);

  glm::mat4 xMat = glm::rotate(glm::mat4(), deg_to_rad(m_core->rotation.x), glm::vec3(1.0f, 0.0f, 0.0f));
  glm::mat4 yMat = glm::rotate(glm::mat4(), deg_to_rad(m_core->rotation.y), glm::vec3(0.0f, 1.0f, 0.0f));

  glm::mat4 rotMat = xMat * yMat;

  facing = at * glm::mat3(yMat);
  at = at * glm::mat3(rotMat);
}

void Camera::updateView()
{
  m_view = glm::lookAt(eye, eye+at, up);
}