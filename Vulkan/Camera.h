#pragma once
#include "VulkanCore.h"
class Camera
{
public:
  Camera(VulkanCore* core, float angle, float aspect, float nearClip, float farClip);
  ~Camera();

  glm::mat4 changeProjection(float angle, float aspect, float nearClip, float farClip);

  void updateView();
  void moveForward(float d) { eye += d*facing; }
  void moveRight(float d) {eye += (d * glm::cross(glm::vec3(0.0f, 1.0f, 0.0f), facing));}
  void moveUp(float d) { eye.y += d; }
  void Orientate();

  glm::mat4 getProjection() { return m_projection; }
  glm::mat4 getView() { return m_view; }
private:
  glm::mat4 m_projection;
  glm::mat4 m_view;

  glm::vec3 eye, at, up, facing;

  VulkanCore* m_core;
};

