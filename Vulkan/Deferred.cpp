#include "Deferred.h"
#include "VulkanCore.h"
#include "Camera.h"

Deferred::~Deferred()
{
  vkDestroyBuffer(*m_core->getDevice(), uniformDataVS.buffer, nullptr);
  vkFreeMemory(*m_core->getDevice(), uniformDataVS.memory, nullptr);
}

void Deferred::prepareUniformBuffers()
{
  shaderStages.push_back(m_core->loadShader("./../data/shaders/deferred.vert.spv", VK_SHADER_STAGE_VERTEX_BIT));
  shaderStages.push_back(m_core->loadShader("./../data/shaders/deferred.frag.spv", VK_SHADER_STAGE_FRAGMENT_BIT));

  blendAttachmentStates = {
    vkTools::initializers::pipelineColorBlendAttachmentState(0xf, VK_FALSE),
    vkTools::initializers::pipelineColorBlendAttachmentState(0xf, VK_FALSE),
    vkTools::initializers::pipelineColorBlendAttachmentState(0xf, VK_FALSE),
    vkTools::initializers::pipelineColorBlendAttachmentState(0xf, VK_FALSE)
  };

  /////////////////////////////////////////////
  // Uniform/Texture Descriptions
  /////////////////////////////////////////////
  m_core->createBuffer(
    VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
    sizeof(uboVS),
    &uboVS,
    &uniformDataVS.buffer,
    &uniformDataVS.memory,
    &uniformDataVS.descriptor);

  bindingDescriptors.emplace_back(
    VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
    VK_SHADER_STAGE_VERTEX_BIT,
    0,
    &uniformDataVS.descriptor);
  bindingDescriptors.emplace_back(
    VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
    VK_SHADER_STAGE_FRAGMENT_BIT,
    1,
    &boundTextures.at(1));
  bindingDescriptors.emplace_back(
    VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
    VK_SHADER_STAGE_FRAGMENT_BIT,
    2,
    &boundTextures.at(2));
  bindingDescriptors.emplace_back(
    VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
    VK_SHADER_STAGE_FRAGMENT_BIT,
    3,
    &boundTextures.at(3));

  poolSizes =
  {
    vkTools::initializers::descriptorPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, m_core->getVertexBuffs().size()),
    vkTools::initializers::descriptorPoolSize(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, m_core->getVertexBuffs().size()*3)
  };

  /////////////////////////////////////////////
  // Buffer Descriptions
  /////////////////////////////////////////////
  std::vector<vkMeshLoader::VertexLayout> vertexLayout = {
    vkMeshLoader::VERTEX_LAYOUT_POSITION,
    vkMeshLoader::VERTEX_LAYOUT_NORMAL,
    vkMeshLoader::VERTEX_LAYOUT_UV,
    vkMeshLoader::VERTEX_LAYOUT_TANGENT,
    vkMeshLoader::VERTEX_LAYOUT_BITANGENT
  };

  bindingDescriptions = { 
    vkTools::initializers::vertexInputBindingDescription(
      VERTEX_BUFFER_BIND_ID,
      vkMeshLoader::vertexSize(vertexLayout),
      VK_VERTEX_INPUT_RATE_VERTEX) 
  };

  attributeDescriptions = {
    // Location 0 : Position
    vkTools::initializers::vertexInputAttributeDescription(
      VERTEX_BUFFER_BIND_ID,
      0,
      VK_FORMAT_R32G32B32_SFLOAT,
      0),
    // Location 1 : Normal
    vkTools::initializers::vertexInputAttributeDescription(
      VERTEX_BUFFER_BIND_ID,
      1,
      VK_FORMAT_R32G32B32_SFLOAT,
      sizeof(float) * 3),
    // Location 2 : TexCoords
    vkTools::initializers::vertexInputAttributeDescription(
      VERTEX_BUFFER_BIND_ID,
      2,
      VK_FORMAT_R32G32_SFLOAT,
      sizeof(float) * 6),
    // Location 3 : Tangent
    vkTools::initializers::vertexInputAttributeDescription(
      VERTEX_BUFFER_BIND_ID,
      3,
      VK_FORMAT_R32G32B32_SFLOAT,
      sizeof(float) * 8),
    // Location 4 : Bitangent
    vkTools::initializers::vertexInputAttributeDescription(
      VERTEX_BUFFER_BIND_ID,
      4,
      VK_FORMAT_R32G32B32_SFLOAT,
      sizeof(float) * 11)
  };

  setInputState();
  updateUniformBuffers();
}

void Deferred::updateUniformBuffers()
{
  uboVS.projectionMatrix = m_core->m_camera->getProjection();

  uboVS.modelViewMatrix = m_core->m_camera->getView();

  glm::mat4 modelMatrix = glm::mat4();

  uboVS.modelViewMatrix *= modelMatrix;

  uboVS.normalMatrix = glm::mat4(glm::transpose(glm::inverse(glm::mat3(uboVS.modelViewMatrix))));

  mapUniform(uniformDataVS.memory, sizeof(uboVS), &uboVS);
}

void Deferred::setupDescriptorSetLayout()
{
  std::vector<VkPushConstantRange> pushConstant(2);
  pushConstant[0].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
  pushConstant[0].offset = 0;
  pushConstant[0].size = sizeof(int);

  pushConstant[1].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
  pushConstant[1].offset = sizeof(int);
  pushConstant[1].size = sizeof(int);

  Shader::setupDescriptorSetLayout(pushConstant.size(), pushConstant.data());
}

void Deferred::setupDescriptorSet()
{
  Shader::setupDescriptorSet(m_core->getVertexBuffs());
}

void Deferred::setupDescriptorPool()
{
  Shader::setupDescriptorPool(m_core->getVertexBuffs().size());
}
