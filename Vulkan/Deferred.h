#pragma once
#include "BaseShader.h"

class Deferred : public Shader
{
public:
  Deferred(VulkanCore *core) : Shader(core) {}
  ~Deferred();

  virtual void prepareUniformBuffers() override;
  virtual void updateUniformBuffers() override;
  virtual void setupDescriptorSetLayout() override;
  virtual void setupDescriptorSet() override;
  virtual void setupDescriptorPool() override;

  struct {
    glm::mat4 projectionMatrix;
    glm::mat4 modelViewMatrix;
    glm::mat4 normalMatrix;
  } uboVS;

  vkTools::UniformData uniformDataVS;

};
