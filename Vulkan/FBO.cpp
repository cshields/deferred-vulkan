#include "FBO.h"
#include "VulkanCore.h"

FBO::FBO(VulkanCore* core, uint32_t width, uint32_t height) : m_width(width), m_height(height)
{
  m_core = core;
  m_depth = nullptr;
}


FBO::~FBO()
{
  delete m_depth;
}

void FBO::createAttachment(VkAttachmentDescription attachmentDesc, VkImageUsageFlagBits usage)
{
  m_attachments.emplace_back();
  m_attachmentDescs.push_back(attachmentDesc);
  createAttachment(attachmentDesc.format, usage);
}

void FBO::createDepth()
{
  // Find a suitable depth format
  VkFormat attDepthFormat;
  VkBool32 validDepthFormat = vkTools::getSupportedDepthFormat(*m_core->getPhysDevice(), &attDepthFormat);
  assert(validDepthFormat);

  m_depth = new FrameBufferAttachment;
  createAttachment(attDepthFormat, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT);

  m_attachmentDescs.emplace_back();
  auto it = m_attachmentDescs.end() - 1;

  it->samples = VK_SAMPLE_COUNT_1_BIT;
  it->loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
  it->storeOp = VK_ATTACHMENT_STORE_OP_STORE;
  it->stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
  it->stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
  it->initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
  it->finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
  it->format = m_depth->format;
}

// Create a frame buffer attachment
void FBO::createAttachment(VkFormat format, VkImageUsageFlagBits usage)
{
  VkImageAspectFlags aspectMask = 0;
  VkImageLayout imageLayout;
  FrameBufferAttachment *attachment;

  if (usage & VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT)
  {
    aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    imageLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    attachment = &m_attachments[m_attachments.size()-1];
  }
  if (usage & VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT)
  {
    aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
    imageLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    attachment = m_depth;
  }

  attachment->format = format;

  assert(aspectMask > 0);

  VkImageCreateInfo image = vkTools::initializers::imageCreateInfo();
  image.imageType = VK_IMAGE_TYPE_2D;
  image.format = format;
  image.extent.width = m_width;
  image.extent.height = m_height;
  image.mipLevels = 1;
  image.arrayLayers = 1;
  image.samples = VK_SAMPLE_COUNT_1_BIT;
  image.tiling = VK_IMAGE_TILING_OPTIMAL;
  image.usage = usage | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;

  VkMemoryAllocateInfo memAlloc = vkTools::initializers::memoryAllocateInfo();

  VkImageViewCreateInfo imageView = vkTools::initializers::imageViewCreateInfo();
  imageView.viewType = VK_IMAGE_VIEW_TYPE_2D;
  imageView.format = format;
  imageView.subresourceRange = {};
  imageView.subresourceRange.aspectMask = aspectMask;
  imageView.subresourceRange.baseMipLevel = 0;
  imageView.subresourceRange.levelCount = 1;
  imageView.subresourceRange.baseArrayLayer = 0;
  imageView.subresourceRange.layerCount = 1;

  VkMemoryRequirements memReqs;

  VkResult err = vkCreateImage(*m_core->getDevice(), &image, nullptr, &attachment->image);
  assert(!err);
  vkGetImageMemoryRequirements(*m_core->getDevice(), attachment->image, &memReqs);
  memAlloc.allocationSize = memReqs.size;
  m_core->getMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &memAlloc.memoryTypeIndex);
  err = vkAllocateMemory(*m_core->getDevice(), &memAlloc, nullptr, &attachment->mem);
  assert(!err);

  err = vkBindImageMemory(*m_core->getDevice(), attachment->image, attachment->mem, 0);
  assert(!err);

  vkTools::setImageLayout(
    m_core->getCmdBuffer(),
    attachment->image,
    aspectMask,
    VK_IMAGE_LAYOUT_UNDEFINED,
    imageLayout);

  imageView.image = attachment->image;
  err = vkCreateImageView(*m_core->getDevice(), &imageView, nullptr, &attachment->view);
  assert(!err);
}

void FBO::prepareFramebuffer()
{
  // Set up separate renderpass with references
  // to the color and depth attachments

  std::vector<VkImageView> attachmentImgs(m_attachments.size());
  std::vector<VkAttachmentReference> colorReferences(m_attachments.size());

  // Init attachment properties
  for (uint32_t i = 0; i < m_attachments.size(); i++)
  {
    attachmentImgs[i] = m_attachments[i].view;
    colorReferences[i] = { i, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };
  }

  VkAttachmentReference* depthReference = nullptr;
  if (m_depth) {
    attachmentImgs.emplace_back(m_depth->view);
    depthReference = new VkAttachmentReference;
    depthReference->attachment = m_attachmentDescs.size()-1;
    depthReference->layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
  }

  VkSubpassDescription subpass = {};
  subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
  subpass.pColorAttachments = colorReferences.data();
  subpass.colorAttachmentCount = colorReferences.size();
  subpass.pDepthStencilAttachment = depthReference;

  VkRenderPassCreateInfo renderPassInfo = {};
  renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
  renderPassInfo.pAttachments = m_attachmentDescs.data();
  renderPassInfo.attachmentCount = m_attachmentDescs.size();
  renderPassInfo.subpassCount = 1;
  renderPassInfo.pSubpasses = &subpass;

  VkResult err = vkCreateRenderPass(*m_core->getDevice(), &renderPassInfo, nullptr, &m_renderPass);
  assert(!err);

  VkFramebufferCreateInfo fbufCreateInfo = {};
  fbufCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
  fbufCreateInfo.pNext = NULL;
  fbufCreateInfo.renderPass = m_renderPass;
  fbufCreateInfo.pAttachments = attachmentImgs.data();
  fbufCreateInfo.attachmentCount = attachmentImgs.size();
  fbufCreateInfo.width = m_width;
  fbufCreateInfo.height = m_height;
  fbufCreateInfo.layers = 1;

  err = vkCreateFramebuffer(*m_core->getDevice(), &fbufCreateInfo, nullptr, &m_frameBuffer);
  assert(!err);

  m_core->flushSetupCommandBuffer();
  m_core->createSetupCommandBuffer();

  delete depthReference;
}

void FBO::prepareFramebuffer(VkRenderPass renderPass)
{
  std::vector<VkImageView> attachmentImgs(m_attachments.size());

  for (uint32_t i = 0; i < m_attachments.size(); i++)
    attachmentImgs[i] = m_attachments[i].view;


  VkFramebufferCreateInfo fbufCreateInfo = {};
  fbufCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
  fbufCreateInfo.pNext = NULL;
  fbufCreateInfo.renderPass = renderPass;
  fbufCreateInfo.pAttachments = attachmentImgs.data();
  fbufCreateInfo.attachmentCount = attachmentImgs.size();
  fbufCreateInfo.width = m_width;
  fbufCreateInfo.height = m_height;
  fbufCreateInfo.layers = 1;

  VkResult err = vkCreateFramebuffer(*m_core->getDevice(), &fbufCreateInfo, nullptr, &m_frameBuffer);
  assert(!err);

  m_core->flushSetupCommandBuffer();
  m_core->createSetupCommandBuffer();
}

// Blit frame buffer attachment to texture target
void FBO::blit(uint32_t sourceID, vkTools::VulkanTexture dest, const VkCommandBuffer &cmdBuff)
{
  // Image memory barrier
  // Transform frame buffer color attachment to transfer source layout
  // Makes sure that writes to the color attachment are finished before
  // using it as source for the blit
  vkTools::setImageLayout(
    cmdBuff,
    m_attachments[sourceID].image,
    VK_IMAGE_ASPECT_COLOR_BIT,
    VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
    VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

  // Image memory barrier
  // Transform texture from shader read (initial layout) to transfer destination layout
  // Makes sure that reads from texture are finished before
  // using it as a transfer destination for the blit
  vkTools::setImageLayout(
    cmdBuff,
    dest.image,
    VK_IMAGE_ASPECT_COLOR_BIT,
    VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
    VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

  // Blit offscreen color buffer to our texture target
  VkImageBlit imgBlit;

  imgBlit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  imgBlit.srcSubresource.mipLevel = 0;
  imgBlit.srcSubresource.baseArrayLayer = 0;
  imgBlit.srcSubresource.layerCount = 1;

  imgBlit.srcOffsets[0] = { 0, 0, 0 };
  imgBlit.srcOffsets[1].x = m_width;
  imgBlit.srcOffsets[1].y = m_height;
  imgBlit.srcOffsets[1].z = 1;

  imgBlit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  imgBlit.dstSubresource.mipLevel = 0;
  imgBlit.dstSubresource.baseArrayLayer = 0;
  imgBlit.dstSubresource.layerCount = 1;

  imgBlit.dstOffsets[0] = { 0, 0, 0 };
  imgBlit.dstOffsets[1].x = dest.width;
  imgBlit.dstOffsets[1].y = dest.height;
  imgBlit.dstOffsets[1].z = 1;

  // Blit from framebuffer image to texture image
  // vkCmdBlitImage does scaling and (if necessary and possible) also does format conversions
  vkCmdBlitImage(
    cmdBuff,
    m_attachments[sourceID].image,
    VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
    dest.image,
    VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
    1,
    &imgBlit,
    VK_FILTER_LINEAR
  );

  // Image memory barrier
  // Transform texture from transfer destination to shader read
  // Makes sure that writes to the texture are finished before
  // using it as the source for a sampler in the shader
  vkTools::setImageLayout(
    cmdBuff,
    dest.image,
    VK_IMAGE_ASPECT_COLOR_BIT,
    VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
    VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

  // Image memory barrier
  // Transform the framebuffer color attachment back
  vkTools::setImageLayout(
    cmdBuff,
    m_attachments[sourceID].image,
    VK_IMAGE_ASPECT_COLOR_BIT,
    VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
    VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
}
