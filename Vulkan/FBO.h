#pragma once
#include "vulkanTextureLoader.hpp"

class VulkanCore;

class FBO
{
public:
  const uint32_t m_width, m_height;

  struct FrameBufferAttachment {
    VkImage image;
    VkDeviceMemory mem;
    VkImageView view;
    VkFormat format;
  };

  FBO(VulkanCore* core, uint32_t width, uint32_t height);
  ~FBO();

  void createAttachment(VkAttachmentDescription attachmentDesc, VkImageUsageFlagBits usage);
  void createDepth();
  void createAttachment(VkFormat format, VkImageUsageFlagBits usage);
  void prepareFramebuffer();
  void prepareFramebuffer(VkRenderPass renderPass);

  void blit(uint32_t sourceID, vkTools::VulkanTexture dest, const VkCommandBuffer &cmdBuff);

  VkRenderPass getRenderPass() { return m_renderPass; }
  VkFramebuffer getFrameBuffer() { return m_frameBuffer; }

  std::vector<VkAttachmentDescription> m_attachmentDescs;

protected:
  VkFramebuffer m_frameBuffer;
  std::vector<FrameBufferAttachment> m_attachments;
  FrameBufferAttachment* m_depth;
  VkRenderPass m_renderPass;
  VulkanCore* m_core;
};

