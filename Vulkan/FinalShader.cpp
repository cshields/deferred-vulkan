#include "FinalShader.h"
#include "VulkanCore.h"
#include "Camera.h"

FinalShader::~FinalShader()
{
}

void FinalShader::prepareUniformBuffers()
{
  shaderStages.push_back(m_core->loadShader("./../data/shaders/final.vert.spv", VK_SHADER_STAGE_VERTEX_BIT));
  shaderStages.push_back(m_core->loadShader("./../data/shaders/final.frag.spv", VK_SHADER_STAGE_FRAGMENT_BIT));

  blendAttachmentStates.push_back(vkTools::initializers::pipelineColorBlendAttachmentState(0xf, VK_FALSE));

  /////////////////////////////////////////////
  // Uniform/Texture Descriptions
  /////////////////////////////////////////////
  for (int i = 0; i < 2; i++) {
    lightFS[i].intensity = glm::vec4(0.5f);
    lightFS[i].colour = glm::vec4(1.f, 1.f, 1.f, 1.f);
  }

  m_core->createBuffer(
    VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
    sizeof(lightFS),
    &lightFS,
    &uniformDataFS.buffer,
    &uniformDataFS.memory,
    &uniformDataFS.descriptor);

  bindingDescriptors.emplace_back(
    VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
    VK_SHADER_STAGE_FRAGMENT_BIT,
    0,
    &uniformDataFS.descriptor);
  bindingDescriptors.emplace_back(
    VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
    VK_SHADER_STAGE_FRAGMENT_BIT,
    1,
    &boundTextures.at(1));
  bindingDescriptors.emplace_back(
    VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
    VK_SHADER_STAGE_FRAGMENT_BIT,
    2,
    &boundTextures.at(2));
  bindingDescriptors.emplace_back(
    VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
    VK_SHADER_STAGE_FRAGMENT_BIT,
    3,
    &boundTextures.at(3));
  bindingDescriptors.emplace_back(
    VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
    VK_SHADER_STAGE_FRAGMENT_BIT,
    4,
    &boundTextures.at(4));
  bindingDescriptors.emplace_back(
    VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
    VK_SHADER_STAGE_FRAGMENT_BIT,
    5,
    &boundTextures.at(5));

  poolSizes =
  {
    vkTools::initializers::descriptorPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1),
    vkTools::initializers::descriptorPoolSize(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 5)
  };

  /////////////////////////////////////////////
  // Buffer Descriptions
  /////////////////////////////////////////////

  std::vector<vkMeshLoader::VertexLayout> quadLayout =
  {
    vkMeshLoader::VERTEX_LAYOUT_POSITION,
    vkMeshLoader::VERTEX_LAYOUT_UV
  };

  bindingDescriptions =
  { vkTools::initializers::vertexInputBindingDescription(
      VERTEX_BUFFER_BIND_ID,
      vkMeshLoader::vertexSize(quadLayout),
      VK_VERTEX_INPUT_RATE_VERTEX) };

  attributeDescriptions = {
    // Location 0 : Position
    vkTools::initializers::vertexInputAttributeDescription(
      VERTEX_BUFFER_BIND_ID,
      0,
      VK_FORMAT_R32G32B32_SFLOAT,
      0),
    // Location 2 : TexCoords
    vkTools::initializers::vertexInputAttributeDescription(
      VERTEX_BUFFER_BIND_ID,
      1,
      VK_FORMAT_R32G32_SFLOAT,
      sizeof(float) * 3)
  };

  setInputState();
  updateUniformBuffers();
}

void FinalShader::updateUniformBuffers()
{
  lightFS[0].pos = m_core->m_camera->getView() * glm::vec4(-4.5f, -5.f, 0.f, 1.f);
  lightFS[1].pos = m_core->m_camera->getView() * glm::vec4(4.5f, -5.f, 0.f, 1.f);

  mapUniform(uniformDataFS.memory, sizeof(lightFS), &lightFS);
}