#pragma once
#include "BaseShader.h"

#define kernelSize 32

class FinalShader : public Shader
{
public:
  FinalShader(VulkanCore* core) : Shader(core) {}
  ~FinalShader();

  virtual void prepareUniformBuffers() override;
  virtual void updateUniformBuffers() override;

  struct {
    glm::vec4 pos;
    glm::vec4 intensity;
    glm::vec4 colour;
    //glm::vec4 kernel[kernelSize];
    //glm::mat4 projection;
  } lightFS[2];

  vkTools::UniformData uniformDataFS;
};

