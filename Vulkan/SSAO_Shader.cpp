#include "SSAO_Shader.h"
#include "VulkanCore.h"
#include "Camera.h"
#include <random>

SSAO_Shader::~SSAO_Shader()
{
}

void SSAO_Shader::prepareUniformBuffers()
{
  shaderStages.push_back(m_core->loadShader("./../data/shaders/final.vert.spv", VK_SHADER_STAGE_VERTEX_BIT));
  shaderStages.push_back(m_core->loadShader("./../data/shaders/SSAO.frag.spv", VK_SHADER_STAGE_FRAGMENT_BIT));

  blendAttachmentStates.push_back(vkTools::initializers::pipelineColorBlendAttachmentState(0xf, VK_FALSE));

  /////////////////////////////////////////////
  // Uniform/Texture Descriptions
  /////////////////////////////////////////////
  std::default_random_engine generator;
  std::uniform_real_distribution<float> distribution(-1.0, 1.0);
  for (int i = 0; i < kernelSize; ++i) {
    ssaoFS.kernel[i].x = distribution(generator),
      ssaoFS.kernel[i].y = distribution(generator),
      ssaoFS.kernel[i].z = distribution(generator)*0.5 + 0.5;
    ssaoFS.kernel[i] = glm::normalize(ssaoFS.kernel[i]);
    float scale = float(i) / float(kernelSize);
    scale = glm::mix(0.1f, 1.0f, scale * scale);
    ssaoFS.kernel[i] *= scale;
  }

  std::vector<glm::vec4> ssaoNoise;
  for (int i = 0; i < 16; i++)
  {
    glm::vec4 noise(
      distribution(generator),
      distribution(generator),
      0.0f, 0.f);
    noise = glm::normalize(noise);
    ssaoNoise.push_back(noise);
  }

  m_core->textureLoader->loadTextureArray(4, 4, VK_FORMAT_R32G32B32A32_SFLOAT, &m_noiseTex, ssaoNoise.data());
  VkDescriptorImageInfo texDescriptorNoise =
    vkTools::initializers::descriptorImageInfo(
      m_noiseTex.sampler,
      m_noiseTex.view,
      VK_IMAGE_LAYOUT_GENERAL);
  boundTextures.emplace(3, texDescriptorNoise);

  m_core->createBuffer(
    VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
    sizeof(ssaoFS),
    &ssaoFS,
    &uniformDataFS.buffer,
    &uniformDataFS.memory,
    &uniformDataFS.descriptor);

  bindingDescriptors.emplace_back(
    VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
    VK_SHADER_STAGE_FRAGMENT_BIT,
    0,
    &uniformDataFS.descriptor);
  bindingDescriptors.emplace_back(
    VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
    VK_SHADER_STAGE_FRAGMENT_BIT,
    1,
    &boundTextures.at(1));
  bindingDescriptors.emplace_back(
    VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
    VK_SHADER_STAGE_FRAGMENT_BIT,
    2,
    &boundTextures.at(2));
  bindingDescriptors.emplace_back(
    VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
    VK_SHADER_STAGE_FRAGMENT_BIT,
    3,
    &boundTextures.at(3));

  poolSizes =
  {
    vkTools::initializers::descriptorPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1),
    vkTools::initializers::descriptorPoolSize(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 3)
  };

  /////////////////////////////////////////////
  // Buffer Descriptions
  /////////////////////////////////////////////

  std::vector<vkMeshLoader::VertexLayout> quadLayout =
  {
    vkMeshLoader::VERTEX_LAYOUT_POSITION,
    vkMeshLoader::VERTEX_LAYOUT_UV
  };

  bindingDescriptions =
  { vkTools::initializers::vertexInputBindingDescription(
    VERTEX_BUFFER_BIND_ID,
    vkMeshLoader::vertexSize(quadLayout),
    VK_VERTEX_INPUT_RATE_VERTEX) };

  attributeDescriptions = {
    // Location 0 : Position
    vkTools::initializers::vertexInputAttributeDescription(
      VERTEX_BUFFER_BIND_ID,
      0,
      VK_FORMAT_R32G32B32_SFLOAT,
      0),
    // Location 2 : TexCoords
    vkTools::initializers::vertexInputAttributeDescription(
      VERTEX_BUFFER_BIND_ID,
      1,
      VK_FORMAT_R32G32_SFLOAT,
      sizeof(float) * 3)
  };

  setInputState();
  updateUniformBuffers();
}

void SSAO_Shader::updateUniformBuffers()
{
  ssaoFS.projection = m_core->m_camera->getProjection();
  mapUniform(uniformDataFS.memory, sizeof(ssaoFS), &ssaoFS);
}