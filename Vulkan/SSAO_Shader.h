#pragma once
#include "BaseShader.h"

#define kernelSize 32

class SSAO_Shader : public Shader
{
private:
  vkTools::VulkanTexture m_noiseTex;

public:
  SSAO_Shader(VulkanCore* core) : Shader(core) {}
  ~SSAO_Shader();

  virtual void prepareUniformBuffers() override;
  virtual void updateUniformBuffers() override;

  struct {
    glm::mat4 projection;
    glm::vec4 kernel[kernelSize];
  } ssaoFS;

  vkTools::UniformData uniformDataFS;
};

