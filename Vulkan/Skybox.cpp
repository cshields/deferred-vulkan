#include "Skybox.h"
#include "VulkanCore.h"
#include "Camera.h"

Skybox::~Skybox()
{
  vkDestroyBuffer(*m_core->getDevice(), uniformDataVS.buffer, nullptr);
  vkFreeMemory(*m_core->getDevice(), uniformDataVS.memory, nullptr);
}

void Skybox::prepareUniformBuffers()
{
  shaderStages.push_back(m_core->loadShader("./../data/shaders/skybox.vert.spv", VK_SHADER_STAGE_VERTEX_BIT));
  shaderStages.push_back(m_core->loadShader("./../data/shaders/skybox.frag.spv", VK_SHADER_STAGE_FRAGMENT_BIT));

  blendAttachmentStates = {
    vkTools::initializers::pipelineColorBlendAttachmentState(0xf, VK_FALSE),
    vkTools::initializers::pipelineColorBlendAttachmentState(0xf, VK_FALSE) ,
    vkTools::initializers::pipelineColorBlendAttachmentState(0xf, VK_FALSE)
  };

  m_faceCulling = VK_CULL_MODE_NONE;
  m_depthWrite = VK_FALSE;

  /////////////////////////////////////////////
  // Uniform/Texture Descriptions
  /////////////////////////////////////////////
  m_core->createBuffer(
    VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
    sizeof(uboVS),
    &uboVS,
    &uniformDataVS.buffer,
    &uniformDataVS.memory,
    &uniformDataVS.descriptor);

  bindingDescriptors.emplace_back(
    VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
    VK_SHADER_STAGE_VERTEX_BIT,
    0,
    &uniformDataVS.descriptor);
  bindingDescriptors.emplace_back(
    VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
    VK_SHADER_STAGE_FRAGMENT_BIT,
    1,
    &boundTextures.at(1));

  poolSizes =
  {
    vkTools::initializers::descriptorPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1),
    vkTools::initializers::descriptorPoolSize(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1)
  };

  /////////////////////////////////////////////
  // Buffer Descriptions
  /////////////////////////////////////////////
  std::vector<vkMeshLoader::VertexLayout> vertexLayout = {
    vkMeshLoader::VERTEX_LAYOUT_POSITION,
  };

  bindingDescriptions = {
    vkTools::initializers::vertexInputBindingDescription(
      VERTEX_BUFFER_BIND_ID,
      vkMeshLoader::vertexSize(vertexLayout),
      VK_VERTEX_INPUT_RATE_VERTEX)
  };

  attributeDescriptions = {
    // Location 0 : Position
    vkTools::initializers::vertexInputAttributeDescription(
      VERTEX_BUFFER_BIND_ID,
      0,
      VK_FORMAT_R32G32B32_SFLOAT,
      0),
  };

  setInputState();
  updateUniformBuffers();
}

void Skybox::updateUniformBuffers()
{
  uboVS.projectionMatrix = m_core->m_camera->getProjection();
  uboVS.modelViewMatrix = glm::mat4(glm::mat3(m_core->m_camera->getView()));

  mapUniform(uniformDataVS.memory, sizeof(uboVS), &uboVS);
}
