#pragma once
#include "BaseShader.h"

class Skybox : public Shader
{
public:
  Skybox(VulkanCore *core) : Shader(core) {}
  ~Skybox();

  virtual void prepareUniformBuffers() override;
  virtual void updateUniformBuffers() override;

  struct {
    glm::mat4 projectionMatrix;
    glm::mat4 modelViewMatrix;
  } uboVS;

  vkTools::UniformData uniformDataVS;

};

