#include "VulkanCore.h"
#include "BaseShader.h"
#include "Deferred.h"
#include "FinalShader.h"
#include "Skybox.h"
#include "SSAO_Shader.h"
#include "Camera.h"
#include "FBO.h"


VulkanCore::VulkanCore() : VulkanExampleBase(ENABLE_VALIDATION)
{
  m_width = 1280;
  m_height = 720;
  title = "Vulkan";
  m_deferred = new Deferred(this);
  m_final = new FinalShader(this);
  m_skybox = new Skybox(this);
  m_ssao = new SSAO_Shader(this);
  m_shaders = { m_deferred, m_final, m_skybox, m_ssao };

  m_camera = new Camera(this, glm::radians(110.0f), (float)m_width / (float)m_height, 0.1f, 256.0f);
}


VulkanCore::~VulkanCore()
{
  for (int i = 0; i < vertexBuffs.size(); i++) {
    vkMeshLoader::freeMeshBufferResources(device, &vertexBuffs[i]);
  }
}

void VulkanCore::buildCommandBuffers()
{
  VkCommandBufferBeginInfo cmdBufInfo = vkTools::initializers::commandBufferBeginInfo();

  VkClearValue clearValues[2];
  clearValues[0].color = defaultClearColor;
  clearValues[1].depthStencil = { 1.0f, 0 };

  VkRenderPassBeginInfo renderPassBeginInfo = vkTools::initializers::renderPassBeginInfo();
  renderPassBeginInfo.renderPass = renderPass;
  renderPassBeginInfo.renderArea.offset.x = 0;
  renderPassBeginInfo.renderArea.offset.y = 0;
  renderPassBeginInfo.renderArea.extent.width = m_width;
  renderPassBeginInfo.renderArea.extent.height = m_height;
  renderPassBeginInfo.clearValueCount = 2;
  renderPassBeginInfo.pClearValues = clearValues;

  VkResult err;

  for (int32_t i = 0; i < drawCmdBuffers.size(); ++i)
  {
    // Set target frame buffer
    renderPassBeginInfo.framebuffer = frameBuffers[i];

    err = vkBeginCommandBuffer(drawCmdBuffers[i], &cmdBufInfo);
    assert(!err);

    vkCmdBeginRenderPass(drawCmdBuffers[i], &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

    // Update dynamic viewport state
    VkViewport viewport = vkTools::initializers::viewport(
    (float)m_width,
    (float)m_height,
    0.f,
    1.f
    );
    vkCmdSetViewport(drawCmdBuffers[i], 0, 1, &viewport);

    // Update dynamic scissor state
    VkRect2D scissor = vkTools::initializers::rect2D(
      m_width,
      m_height,
      0,
      0
    );
    vkCmdSetScissor(drawCmdBuffers[i], 0, 1, &scissor);

    // Bind descriptor sets describing shader binding points
    vkCmdBindDescriptorSets(drawCmdBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, *m_final->getPipelineLayout(), 0, 1, m_final->getSet(), 0, NULL);

    // Bind the rendering pipeline
    vkCmdBindPipeline(drawCmdBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, *m_final->getPipeline());
    VkDeviceSize offsets[1] = { 0 };
    vkCmdBindVertexBuffers(drawCmdBuffers[i], VERTEX_BUFFER_BIND_ID, 1, &m_quad.vertices.buf, offsets);     // Bind mesh vertices
    vkCmdBindIndexBuffer(drawCmdBuffers[i], m_quad.indices.buf, 0, VK_INDEX_TYPE_UINT32);    // Bind mesh indices
    vkCmdDrawIndexed(drawCmdBuffers[i], m_quad.indexCount, 1, 0, 0, 1);    // Draw indexed mesh

    vkCmdEndRenderPass(drawCmdBuffers[i]);

    // Ending the render pass will add an implicit barrier transitioning the frame buffer color attachment to 
    // VK_IMAGE_LAYOUT_PRESENT_SRC_KHR for presenting it to the windowing system

    err = vkEndCommandBuffer(drawCmdBuffers[i]);
    assert(!err);
  }
}

void VulkanCore::draw()
{
  VkResult err;
  VkSemaphore presentCompleteSemaphore;
  VkSemaphoreCreateInfo presentCompleteSemaphoreCreateInfo =
    vkTools::initializers::semaphoreCreateInfo(VK_FENCE_CREATE_SIGNALED_BIT);

  err = vkCreateSemaphore(device, &presentCompleteSemaphoreCreateInfo, nullptr, &presentCompleteSemaphore);
  assert(!err);

  // Get next image in the swap chain (back/front buffer)
  err = swapChain.acquireNextImage(presentCompleteSemaphore, &currentBuffer);
  assert(!err);

  // Gather command buffers to be sumitted to the queue
  std::vector<VkCommandBuffer> submitCmdBuffers = {
    offScreenCmdBuffer,
    drawCmdBuffers[currentBuffer],
  };

  VkSubmitInfo submitInfo = vkTools::initializers::submitInfo();
  submitInfo.waitSemaphoreCount = 1;
  submitInfo.pWaitSemaphores = &presentCompleteSemaphore;
  submitInfo.commandBufferCount = submitCmdBuffers.size();
  submitInfo.pCommandBuffers = submitCmdBuffers.data();

  // Submit to the graphics queue
  err = vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);
  assert(!err);

  // Present the current buffer to the swap chain
  // This will display the image
  err = swapChain.queuePresent(queue, currentBuffer);
  assert(!err);

  vkDestroySemaphore(device, presentCompleteSemaphore, nullptr);

  err = vkQueueWaitIdle(queue);
  assert(!err);
}

void VulkanCore::prepareVertices()
{
  std::vector<vkMeshLoader::VertexLayout> vertexLayout =
  {
    vkMeshLoader::VERTEX_LAYOUT_POSITION,
    vkMeshLoader::VERTEX_LAYOUT_NORMAL,
    vkMeshLoader::VERTEX_LAYOUT_UV,
    vkMeshLoader::VERTEX_LAYOUT_TANGENT,
    vkMeshLoader::VERTEX_LAYOUT_BITANGENT
  };
  loadMesh("./../data/models/Sponza/sponza.obj", vertexBuffs, vertexLayout, 0.01f, aiProcess_FlipWindingOrder | aiProcess_Triangulate | aiProcess_PreTransformVertices | aiProcess_GenSmoothNormals | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
  vertexLayout.resize(1);
  loadMesh("./../data/models/cube.obj", &m_cube, vertexLayout, 1.0f);
  std::vector<vkMeshLoader::VertexLayout> quadLayout =
  {
    vkMeshLoader::VERTEX_LAYOUT_POSITION,
    vkMeshLoader::VERTEX_LAYOUT_UV
  };

  loadMesh("./../data/models/screen.obj", &m_quad, quadLayout, 1.0f);
}

// Find and create a compute capable device queue
void VulkanCore::getComputeQueue()
{
  uint32_t queueIndex = 0;
  uint32_t queueCount;
  vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueCount, NULL);
  assert(queueCount >= 1);

  std::vector<VkQueueFamilyProperties> queueProps;
  queueProps.resize(queueCount);
  vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueCount, queueProps.data());

  for (queueIndex = 0; queueIndex < queueCount; queueIndex++)
  {
    if (queueProps[queueIndex].queueFlags & VK_QUEUE_COMPUTE_BIT)
      break;
  }
  assert(queueIndex < queueCount);

  VkDeviceQueueCreateInfo queueCreateInfo = {};
  queueCreateInfo.queueFamilyIndex = queueIndex;
  queueCreateInfo.queueCount = 1;
  vkGetDeviceQueue(device, queueIndex, 0, &computeQueue);
}


void VulkanCore::prepareTextureTargets()
{
  createSetupCommandBuffer();

  textureLoader->loadTexture(m_width, m_height, VK_FORMAT_R32G32B32A32_SFLOAT, &textureTargets.position, setupCmdBuffer);
  textureLoader->loadTexture(m_width, m_height, VK_FORMAT_R32G32B32A32_SFLOAT, &textureTargets.normal, setupCmdBuffer);
  textureLoader->loadTexture(m_width, m_height, VK_FORMAT_R8G8B8A8_UNORM, &textureTargets.colour, setupCmdBuffer);
  textureLoader->loadTexture(m_width, m_height, VK_FORMAT_R8G8B8A8_UNORM, &textureTargets.specular, setupCmdBuffer);
  textureLoader->loadTexture(m_width, m_height, VK_FORMAT_R32_SFLOAT, &textureTargets.occlusionFactor, setupCmdBuffer);

  flushSetupCommandBuffer();

  // Image descriptor for the offscreen texture targets
  VkDescriptorImageInfo texDescriptorPosition =
    vkTools::initializers::descriptorImageInfo(
      textureTargets.position.sampler,
      textureTargets.position.view,
      VK_IMAGE_LAYOUT_GENERAL);

  VkDescriptorImageInfo texDescriptorNormal =
    vkTools::initializers::descriptorImageInfo(
      textureTargets.normal.sampler,
      textureTargets.normal.view,
      VK_IMAGE_LAYOUT_GENERAL);

  VkDescriptorImageInfo texDescriptorColour =
    vkTools::initializers::descriptorImageInfo(
      textureTargets.colour.sampler,
      textureTargets.colour.view,
      VK_IMAGE_LAYOUT_GENERAL);

  VkDescriptorImageInfo texDescriptorSpecular =
    vkTools::initializers::descriptorImageInfo(
      textureTargets.specular.sampler,
      textureTargets.specular.view,
      VK_IMAGE_LAYOUT_GENERAL);

  VkDescriptorImageInfo texDescriptorOcclusion =
    vkTools::initializers::descriptorImageInfo(
      textureTargets.occlusionFactor.sampler,
      textureTargets.occlusionFactor.view,
      VK_IMAGE_LAYOUT_GENERAL);

  m_final->addTexInfo(1, texDescriptorPosition);
  m_final->addTexInfo(2, texDescriptorNormal);
  m_final->addTexInfo(3, texDescriptorColour);
  m_final->addTexInfo(4, texDescriptorSpecular);
  m_final->addTexInfo(5, texDescriptorOcclusion);

  m_ssao->addTexInfo(1, texDescriptorPosition);
  m_ssao->addTexInfo(2, texDescriptorNormal);
}

// Prepare a new framebuffer for offscreen rendering
// The contents of this framebuffer are then
// blitted to our render target
void VulkanCore::prepareOffscreenFramebuffer()
{
  m_offScreenFrameBuf = new FBO(this, m_width, m_height);
  // Color attachments

  // (World space) Positions
  m_offScreenFrameBuf->createAttachment(
    vkTools::initializers::attachmentDescription(VK_FORMAT_R32G32B32A32_SFLOAT),
    VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT);

  // (World space) Normals
  m_offScreenFrameBuf->createAttachment(
    vkTools::initializers::attachmentDescription(VK_FORMAT_R32G32B32A32_SFLOAT),
    VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT);

  // Diffuse Texture
  m_offScreenFrameBuf->createAttachment(
    vkTools::initializers::attachmentDescription(VK_FORMAT_R8G8B8A8_UNORM),
    VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT);

  // Specular Texture
  m_offScreenFrameBuf->createAttachment(
    vkTools::initializers::attachmentDescription(VK_FORMAT_R8G8B8A8_UNORM),
    VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT);

  m_offScreenFrameBuf->createDepth();

  m_offScreenFrameBuf->prepareFramebuffer();

  m_ssaoFrameBuf = new FBO(this, m_width, m_height);
  // Color attachments

  // Occulsion Factor
  m_ssaoFrameBuf->createAttachment(
    vkTools::initializers::attachmentDescription(VK_FORMAT_R32_SFLOAT),
    VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT);

  m_ssaoFrameBuf->prepareFramebuffer();
}

// Build command buffer for rendering the scene to the offscreen frame buffer 
// and blitting it to the different texture targets
void VulkanCore::buildDeferredCommandBuffer()
{
  VkResult err;

  // Create separate command buffer for offscreen 
  // rendering
  if (offScreenCmdBuffer == VK_NULL_HANDLE)
  {
    VkCommandBufferAllocateInfo cmd = vkTools::initializers::commandBufferAllocateInfo(
      cmdPool,
      VK_COMMAND_BUFFER_LEVEL_PRIMARY,
      1);
    VkResult vkRes = vkAllocateCommandBuffers(device, &cmd, &offScreenCmdBuffer);
    assert(!vkRes);
  }

  VkCommandBufferBeginInfo cmdBufInfo = vkTools::initializers::commandBufferBeginInfo();

  // Clear values for all attachments written in the fragment shader
  std::vector<VkClearValue> clearValues(5);
  clearValues[0].color = { { 0.0f, 0.0f, 0.0f, 0.0f } };
  clearValues[1].color = { { 0.0f, 0.0f, 0.0f, 0.0f } };
  clearValues[2].color = { { 0.0f, 0.0f, 0.0f, 0.0f } };
  clearValues[3].color = { { 0.0f, 0.0f, 0.0f, 0.0f } };
  clearValues[4].depthStencil = { 1.0f, 0 };

  VkRenderPassBeginInfo renderPassBeginInfo = vkTools::initializers::renderPassBeginInfo();
  renderPassBeginInfo.renderPass = m_offScreenFrameBuf->getRenderPass();
  renderPassBeginInfo.framebuffer = m_offScreenFrameBuf->getFrameBuffer();
  renderPassBeginInfo.renderArea.extent.width = m_offScreenFrameBuf->m_width;
  renderPassBeginInfo.renderArea.extent.height = m_offScreenFrameBuf->m_height;
  renderPassBeginInfo.clearValueCount = clearValues.size();
  renderPassBeginInfo.pClearValues = clearValues.data();

  err = vkBeginCommandBuffer(offScreenCmdBuffer, &cmdBufInfo);
  assert(!err);

  vkCmdBeginRenderPass(offScreenCmdBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

  VkViewport viewport = vkTools::initializers::viewport(
    (float)m_offScreenFrameBuf->m_width,
    (float)m_offScreenFrameBuf->m_height,
    0.0f,
    1.0f);
  vkCmdSetViewport(offScreenCmdBuffer, 0, 1, &viewport);

  VkRect2D scissor = vkTools::initializers::rect2D(
    m_offScreenFrameBuf->m_width,
    m_offScreenFrameBuf->m_height,
    0,
    0);
  vkCmdSetScissor(offScreenCmdBuffer, 0, 1, &scissor);

  VkDeviceSize offsets[1] = { 0 };
  // render skybox here
  vkCmdBindDescriptorSets(offScreenCmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, *m_skybox->getPipelineLayout(), 0, 1, m_skybox->getSet(), 0, NULL);
  vkCmdBindPipeline(offScreenCmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, *m_skybox->getPipeline());

  vkCmdBindVertexBuffers(offScreenCmdBuffer, VERTEX_BUFFER_BIND_ID, 1, &m_cube.vertices.buf, offsets);
  vkCmdBindIndexBuffer(offScreenCmdBuffer, m_cube.indices.buf, 0, VK_INDEX_TYPE_UINT32);
  vkCmdDrawIndexed(offScreenCmdBuffer, m_cube.indexCount, 1, 0, 0, 0);

  // render scene here
  vkCmdBindPipeline(offScreenCmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, *m_deferred->getPipeline());
  for (int i = 0; i < vertexBuffs.size(); i++) {
    vkCmdBindDescriptorSets(offScreenCmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, *m_deferred->getPipelineLayout(), 0, 1, m_deferred->getSet(i), 0, NULL);
    
    int push[2];
    push[0] = vertexBuffs[i].m_specTex.image != VK_NULL_HANDLE ? 1 : 0;
    push[1] = vertexBuffs[i].m_normTex.image != VK_NULL_HANDLE ? 1 : 0;
    vkCmdPushConstants(offScreenCmdBuffer, *m_deferred->getPipelineLayout(), VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(int)*2, &push);

    vkCmdBindVertexBuffers(offScreenCmdBuffer, VERTEX_BUFFER_BIND_ID, 1, &vertexBuffs[i].vertices.buf, offsets);
    vkCmdBindIndexBuffer(offScreenCmdBuffer, vertexBuffs[i].indices.buf, 0, VK_INDEX_TYPE_UINT32);
    vkCmdDrawIndexed(offScreenCmdBuffer, vertexBuffs[i].indexCount, 1, 0, 0, 0);
  }

  vkCmdEndRenderPass(offScreenCmdBuffer);

  m_offScreenFrameBuf->blit(0, textureTargets.position, offScreenCmdBuffer);
  m_offScreenFrameBuf->blit(1, textureTargets.normal, offScreenCmdBuffer);
  m_offScreenFrameBuf->blit(2, textureTargets.colour, offScreenCmdBuffer);
  m_offScreenFrameBuf->blit(3, textureTargets.specular, offScreenCmdBuffer);

  // Clear values for all attachments written in the fragment shader
  clearValues.resize(1);
  clearValues[0].color = { { 0.0f, 0.0f, 0.0f, 0.0f } };

  renderPassBeginInfo.renderPass = m_ssaoFrameBuf->getRenderPass();
  renderPassBeginInfo.framebuffer = m_ssaoFrameBuf->getFrameBuffer();
  renderPassBeginInfo.renderArea.extent.width = m_ssaoFrameBuf->m_width;
  renderPassBeginInfo.renderArea.extent.height = m_ssaoFrameBuf->m_height;
  renderPassBeginInfo.clearValueCount = clearValues.size();
  renderPassBeginInfo.pClearValues = clearValues.data();

  vkCmdBeginRenderPass(offScreenCmdBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

  // render occlussion here
  vkCmdBindDescriptorSets(offScreenCmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, *m_ssao->getPipelineLayout(), 0, 1, m_ssao->getSet(), 0, NULL);
  vkCmdBindPipeline(offScreenCmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, *m_ssao->getPipeline());

  vkCmdBindVertexBuffers(offScreenCmdBuffer, VERTEX_BUFFER_BIND_ID, 1, &m_quad.vertices.buf, offsets);
  vkCmdBindIndexBuffer(offScreenCmdBuffer, m_quad.indices.buf, 0, VK_INDEX_TYPE_UINT32);
  vkCmdDrawIndexed(offScreenCmdBuffer, m_quad.indexCount, 1, 0, 0, 1);

  vkCmdEndRenderPass(offScreenCmdBuffer);

  m_ssaoFrameBuf->blit(0, textureTargets.occlusionFactor, offScreenCmdBuffer);

  err = vkEndCommandBuffer(offScreenCmdBuffer);
  assert(!err);
}

void VulkanCore::setupDescriptorPool()
{
  for (auto& shader : m_shaders)
      shader->setupDescriptorPool();
}

void VulkanCore::setupDescriptorSetLayout()
{
  for (auto& shader : m_shaders)
    shader->setupDescriptorSetLayout();
}

void VulkanCore::setupDescriptorSet()
{
  for (auto& shader : m_shaders)
      shader->setupDescriptorSet();
}

void VulkanCore::preparePipelines()
{
  m_skybox->preparePipelines(m_offScreenFrameBuf->getRenderPass());
  m_deferred->preparePipelines(m_offScreenFrameBuf->getRenderPass());
  m_ssao->preparePipelines(m_ssaoFrameBuf->getRenderPass());
  m_final->preparePipelines(renderPass);
}

void VulkanCore::prepareUniformBuffers()
{
  m_deferred->addTexInfo(1, vertexBuffs[0].m_diffTex.texDescriptor);
  m_deferred->addTexInfo(2, vertexBuffs[0].m_specTex.texDescriptor);
  m_deferred->addTexInfo(3, vertexBuffs[0].m_normTex.texDescriptor);

  textureLoader->loadCubemap("./../data/textures/cubemap_yokohama.ktx",
    VK_FORMAT_BC3_UNORM_BLOCK,
    &m_skyTex
  );
  VkDescriptorImageInfo texDescriptor =
    vkTools::initializers::descriptorImageInfo(
      m_skyTex.sampler,
      m_skyTex.view,
      VK_IMAGE_LAYOUT_GENERAL);
  m_skybox->addTexInfo(1, texDescriptor);

  for (auto& shader : m_shaders)
    shader->prepareUniformBuffers();
}

void VulkanCore::updateUniformBuffers()
{
  for (auto& shader : m_shaders)
    shader->updateUniformBuffers();
}

void VulkanCore::prepare()
{
  VulkanExampleBase::prepare();
  prepareVertices();
  prepareOffscreenFramebuffer();
  prepareTextureTargets();
  prepareUniformBuffers();
  setupDescriptorSetLayout();
  preparePipelines();
  setupDescriptorPool();
  setupDescriptorSet();
  buildCommandBuffers();
  buildDeferredCommandBuffer();
  prepared = true;
}

void VulkanCore::render()
{
  if (!prepared)
    return;
  vkDeviceWaitIdle(device);
  draw();
  vkDeviceWaitIdle(device);

}

void VulkanCore::viewChanged()
{
  // This function is called by the base example class 
  // each time the view is changed by user input
}

void VulkanCore::update()
{
  if (hasFocus)
    SetCursorPos(screenCentre.x, screenCentre.y);
  if (forward) m_camera->moveForward(6.f*frameTimer);
  if (backward) m_camera->moveForward(-6.f*frameTimer);
  if (right) m_camera->moveRight(-6.f*frameTimer);
  if (left) m_camera->moveRight(6.f*frameTimer);
  if (up) m_camera->moveUp(-6.f*frameTimer);
  if (down) m_camera->moveUp(6.f*frameTimer);
  m_camera->Orientate();
  updateUniformBuffers();

  m_camera->updateView();
}

