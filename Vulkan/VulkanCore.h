#pragma once
#include "vulkanexamplebase.h"

#define ENABLE_VALIDATION 0
#define VERTEX_BUFFER_BIND_ID 0

class Shader;
class Camera;
class FBO;

class VulkanCore : public VulkanExampleBase
{
public:

  struct {
    vkTools::VulkanTexture position;
    vkTools::VulkanTexture normal;
    vkTools::VulkanTexture colour;
    vkTools::VulkanTexture specular;
    vkTools::VulkanTexture normalMap;
    vkTools::VulkanTexture occlusionFactor;
  }textureTargets;

  VulkanCore();
  ~VulkanCore();

  void buildCommandBuffers();
  void draw();
  void prepareVertices();
  void setupDescriptorPool();
  void setupDescriptorSetLayout();
  void setupDescriptorSet();
  void preparePipelines();
  void prepareUniformBuffers();
  void updateUniformBuffers();
  void prepare();
  virtual void viewChanged() override;
  virtual void update() override;
  virtual void render() override;

  void prepareTextureTargets();
  void prepareOffscreenFramebuffer();
  void buildDeferredCommandBuffer();
  void getComputeQueue();

  VkDevice* getDevice() { return &device; }
  VkPhysicalDevice* getPhysDevice() { return &physicalDevice; }
  VkQueue* getQueue() { return &queue; }
  VkCommandPool* getCmdPool() { return &cmdPool; }
  VkPhysicalDeviceMemoryProperties* getMemProperties() { return &deviceMemoryProperties; }
  uint32_t getWidth() { return m_width; }
  uint32_t getHeight() { return m_height; }
  VkRenderPass* getRenderPass() { return &renderPass; }
  VkCommandBuffer getCmdBuffer() { return setupCmdBuffer; }
  const std::vector<vkMeshLoader::MeshBuffer>& getVertexBuffs() { return vertexBuffs; }
  Camera* m_camera;

protected:

  std::vector<vkMeshLoader::MeshBuffer> vertexBuffs;
  vkMeshLoader::MeshBuffer m_quad;
  vkMeshLoader::MeshBuffer m_cube;
  vkTools::VulkanTexture m_textue;
  vkTools::VulkanTexture m_skyTex;

  VkCommandBuffer offScreenCmdBuffer = VK_NULL_HANDLE;
  VkCommandBuffer ssaoBuffer = VK_NULL_HANDLE;
  VkQueue computeQueue;

  std::vector<Shader*> m_shaders;
  //Shader* m_phong;
  Shader* m_deferred;
  Shader* m_final;
  Shader* m_skybox;
  Shader* m_ssao;

  FBO* m_offScreenFrameBuf;
  FBO* m_ssaoFrameBuf;
};

