#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
//#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "VulkanCore.h"

VulkanCore *vulkanExample;

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (vulkanExample != NULL)
	{
		vulkanExample->handleMessages(hWnd, uMsg, wParam, lParam);
	}
	return (DefWindowProc(hWnd, uMsg, wParam, lParam));
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR pCmdLine, int nCmdShow)
{
  vulkanExample = new VulkanCore();
  vulkanExample->setupWindow(hInstance, WndProc);

  vulkanExample->initSwapchain();
  vulkanExample->prepare();
  vulkanExample->renderLoop();
  delete(vulkanExample);

	return 0;
}