#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 inTexCoords;

layout (location = 0) out float outOcclusionFactor;

layout (binding = 0) uniform AO
{
  mat4 projection;
  vec4 kernel[32];
} ssao;

layout (binding = 1) uniform sampler2D texPos;
layout (binding = 2) uniform sampler2D texNorm;
layout (binding = 3) uniform sampler2D rndNormals;

const vec2 noiseScale = vec2(1280.0/4.0, 720.0/4.0);

void main(){
  vec3 norm = texture(texNorm, inTexCoords).xyz;
  vec3 pos = texture(texPos, inTexCoords).xyz;
      
	vec3 rndVec = texture(rndNormals, inTexCoords * noiseScale).xyz;
	vec3 tangent = normalize(rndVec - norm * dot(rndVec, norm));
	vec3 bitangent = cross(norm, tangent);
	mat3 TBN = mat3(tangent, bitangent, norm);  
	float radius = 0.1f;

  float occlusion = 0.f;
	for(int i = 0; i < 32; ++i)
	{
		// get sample position
		vec3 Sample = TBN * ssao.kernel[i].rgb; // From tangent to view-space
		Sample = pos + Sample * radius; 
    
		vec4 offset = vec4(Sample, 1.0);
		offset = ssao.projection * offset; // from view to clip-space
		offset.xy /= offset.w; // perspective divide
		offset.xy = offset.xy * 0.5 + 0.5; // transform to range 0.0 - 1.0  

		vec3 sampledPos = texture(texPos, offset.xy).xyz;

		float rangeCheck = smoothstep(0.0, 1.0, radius / abs(pos.z - sampledPos.z));
		occlusion += step(Sample.z, sampledPos.z) * rangeCheck;    
	}  

	outOcclusionFactor = 1.f - occlusion / 32.f;
}

