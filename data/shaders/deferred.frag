#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNorm;
layout (location = 2) in vec2 inTexCoords;

layout (location = 0) out vec3 outPos;
layout (location = 1) out vec3 outNorm;
layout (location = 2) out vec4 outColour;
layout (location = 3) out vec4 outSpecular;

layout (push_constant) uniform pushConstants
{
  int hasSpecMap;
}push;

layout (binding = 1) uniform sampler2D texColour;
layout (binding = 2) uniform sampler2D texSpecular;

void main() 
{
  vec4 colour = texture(texColour, inTexCoords);
  if (colour.a == 0.f)
    discard;
  outPos = inPos;
  outNorm = inNorm;
  outColour = colour;
  if (push.hasSpecMap != 0)
    outSpecular = texture(texSpecular, inTexCoords);
}