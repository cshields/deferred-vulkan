#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNorm;
layout (location = 2) in vec2 inTexCoords;

layout (binding = 0) uniform UBO 
{
	mat4 projectionMatrix;
	mat4 modelViewMatrix;
  mat4 normalMatrix;
} ubo;

layout (location = 0) out vec3 outPos;
layout (location = 1) out vec3 outNorm;
layout (location = 2) out vec2 outTexCoords;

void main() 
{
  vec4 vertexPosition = ubo.modelViewMatrix * vec4(inPos.xyz, 1.0);
  outPos = vertexPosition.xyz;
  
  outNorm = mat3(ubo.normalMatrix) * inNorm;
  outTexCoords = inTexCoords; 
	
	gl_Position = ubo.projectionMatrix * vertexPosition;
}
