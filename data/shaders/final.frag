#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 inTexCoords;

layout (location = 0) out vec4 outFragColor;

struct light
{
  vec4 pos;
  vec4 intensity;
  vec4 colour;
};

layout (binding = 0) uniform UBO
{
  light lights[4];
} ubo;

layout (binding = 1) uniform sampler2D texPos;
layout (binding = 2) uniform sampler2D texNorm;
layout (binding = 3) uniform sampler2D texColour;
layout (binding = 4) uniform sampler2D texSpecular;
layout (binding = 5) uniform sampler2D occlusionTex;

void main() 
{  
  vec3 colour = vec3(1.);
  vec3 pos = texture(texPos, inTexCoords).xyz;    
  vec3 norm = texture(texNorm, inTexCoords).xyz;
  
  if (length(norm) > 0.){
    colour = vec3(0.f);
    for(int lightNum = 0; lightNum < 4; lightNum++){
      vec3 result = vec3(0.f);
      vec3 lightDir = normalize(ubo.lights[lightNum].pos.xyz - pos);
      
     float ao = 0.f;
      vec2 pixSize = 1.f/vec2(1280.f, 720.f);
      ivec2 blurSize = ivec2(2);
      for (int i = -blurSize.x; i < blurSize.x; ++i) {
        for (int j = -blurSize.y; j < blurSize.y; ++j) {
          ao += texture(occlusionTex, inTexCoords + pixSize * vec2(i, j)).r;
        }
      }
      ao /= (blurSize.x * 2) * (blurSize.y * 2);
      
      // diffuse
      result = ubo.lights[lightNum].colour.rgb * ubo.lights[lightNum].intensity.rgb;
      
      float direction = dot(lightDir, norm);
      result = max(direction * result, result * 0.1) * ao;
      
      // Specular 
      if (direction > 0.){
        vec3 spec = texture(texSpecular, inTexCoords).rgb;
        vec3 R = reflect(-lightDir, norm);
        result += spec * ubo.lights[lightNum].intensity.rgb * pow(max(dot(R,normalize(-pos)),0.), 1.);        
      }
      colour += result;
    }
  }
  
  colour *= texture(texColour, inTexCoords).rgb;

  outFragColor = vec4(colour, 1.f);
}